<?php

require_once 'config.php';
require_once 'classes/Game.php';
require_once 'classes/Dice.php';
require_once 'classes/Player.php';
require_once 'classes/Round.php';
require_once 'classes/Knight.php';
require_once 'classes/Arena.php';
require_once 'classes/Logger.php';
require_once 'classes/Item.php';
require_once 'classes/MagicShield.php';
require_once 'classes/MagicWand.php';

// Determining the transmitted number of knights and checking its validity
$knightsCount = \TurnBasedGame\Config::DEFAULT_KNIGHT_NUMBER;

if (!isset($argv[1])) {
    echo 'The number of knights is not indicated. The number of knights is set to ' . $knightsCount . "\n";
} elseif (!ctype_digit(strval(($argv[1])))) {  // check for the ability to cast the argument to an integer number
    echo 'Could not determine the transferred number of knights. The number of knights is set to ' . $knightsCount . "\n";
} else {
    $knightsCount = $argv[1];
}

$logger = new TurnBasedGame\GameTools\Logger('log.txt');

// Create an arena
$arena = new TurnBasedGame\GameObjects\Arena($logger);

// Get the given names from the file
$knightNames = [];

if (file_exists('names.txt') && \TurnBasedGame\Config::USE_HUMAN_NAMES) {
    $knightNames = explode("\n", file_get_contents('names.txt'));
}

// Array of available magic items
$item_array = ['TurnBasedGame\GameObjects\MagicShield', 'TurnBasedGame\GameObjects\MagicWand'];

// Create an array of knights
for ($i = 0; $i < $knightsCount; $i++) {
    $knightName = isset($knightNames[$i]) ? trim($knightNames[$i]) : 'Knight ' . $i;
    // Create a knight, set his name
    $knight = new TurnBasedGame\GameObjects\Knight($knightName);
    // We stuff the knight's bag with objects in a random order
    $items_count = rand(0, count($item_array));
    if ($items_count > 0) {
        $items = array_rand($item_array, $items_count);
        if (!is_array($items)) {
            $items = [$items];
        }
        foreach ($items as $item_index) {
            $knight->addItem(new $item_array[$item_index]());
        }
    }
    // Add the created knight to the arena
    $arena->addPlayer($knight);
}

// Holding rounds until the winner is decided
do {
    $arena->turnIteration();

    if (\TurnBasedGame\Config::SHOW_BATTLELOG_IN_CONSOLE) echo '-------------------------------------------------' . "\n";
    //sleep(1);
} while ($arena->getWinner() === null);

$logger->log('Winner: ' . $arena->getWinner()->name . "\n");

// Display statistics for all created knights
$arena->getStatistic();


