<?php

namespace TurnBasedGame\GameObjects;

use TurnBasedGame\GameTools;

/**
 * Class Arena
 * @package TurnBasedGame\GameObjects
 */
class Arena implements Game
{
    use Round;

    /**
     * @var array
     */
    public $knights = [];
    /**
     * @var GameTools\Logger|null
     */
    private $logger = null;

    /**
     * Arena constructor.
     * @param GameTools\Logger $logger
     */
    public function __construct(GameTools\Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Statistics output for all knights at the end of the game
     */
    public function getStatistic()
    {
        foreach ($this->knights as $knight) {
            echo $knight->name . '. ' . ($knight->isAlive() ? 'HP: ' . $knight->getHp() : 'dead') . "\n";
        }
    }

    /**
     * Determine the winner
     * @return mixed|null
     */
    public function getWinner()
    {
        $alive_knights = 0;
        $glad = null;
        foreach ($this->knights as $knight) {
            if ($knight->isAlive()) {
                $alive_knights++;
                $glad = $knight;
            }
        }
        return ($alive_knights == 1) ? $glad : null;
    }

    /**
     * Turn iteration, which includes the preparation and conduct of the round
     */
    public function turnIteration(): void
    {
        shuffle($this->knights);

        $i = 0;
        $knight1 = null;
        $knight2 = null;

        do {
            // look for a couple of living knights
            if ($knight1 === null) {
                if ($this->knights[$i]->isAlive()) {
                    $knight1 = $this->knights[$i];
                }
                $i++;
            } elseif ($knight2 === null) {
                if ($this->knights[$i]->isAlive()) {
                    $knight2 = $this->knights[$i];
                }
                $i++;
            }

            // draw a duel between the living knights
            if ($knight1 !== null && $knight2 !== null) {
                $this->setPlayers($knight1, $knight2);
                $roundResult = $this->beforeRound()->duringRound()->afterRound()->roundResult();
                $this->logger->log($roundResult);
                $knight1 = null;
                $knight2 = null;
            }

        } while (isset($this->knights[$i])); // Check if all knights have passed
    }

    /**
     * Add a knight to the arena
     * @param Player $knight
     */
    public function addPlayer(Player $knight): void
    {
        $this->knights[] = $knight;
    }


}