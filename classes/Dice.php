<?php

namespace TurnBasedGame\GameTools;

/**
 * Class Dice
 * @package TurnBasedGame\GameTools
 */
class Dice
{
    /**
     * @return int
     */
    static public function roll(): int
    {
        return rand(1, 6);
    }
}

