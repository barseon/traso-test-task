<?php

namespace TurnBasedGame\GameObjects;

interface Game {
    public function getStatistic();
    public function getWinner();
    public function turnIteration();
    public function addPlayer(Player $player);
}