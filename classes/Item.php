<?php

namespace TurnBasedGame\GameObjects;

/**
 * Class Item
 * @package TurnBasedGame\GameObjects
 */
abstract class Item
{
    /**
     * @var bool
     */
    public $single_use = false;
    /**
     * @var
     */
    protected $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param $enemyDamage
     * @param $ownerDamage
     * @return mixed
     */
    abstract public function modificateDamage(&$enemyDamage, &$ownerDamage); //модификация урона от предмета
}

