<?php

namespace TurnBasedGame\GameObjects;

use TurnBasedGame\GameTools;

/**
 * Class Knight
 * @package TurnBasedGame\GameObjects
 */
class Knight extends Player
{
    /**
     * @var array
     */
    public $inventory = [];

    /**
     * @return int
     */
    public function applyHit(): int
    {
        return GameTools\Dice::roll();
    }

    /**
     * @param $damage
     */
    public function takeDamage($damage): void
    {
        $this->hp -= $damage;
        if ($this->hp < 0) { // Проверяем чтобы у гладиатора HP не было менее 0
            $this->hp = 0;
        }
    }

    /**
     * Adding an item to inventory
     * @param Item $item
     */
    public function addItem(Item $item): void
    {
        $this->inventory[] = $item;
    }

    /**
     * Check if the knight wants to use the item
     * @return bool
     */
    public function wantUseItem(): bool
    {
        return (count($this->inventory) > 0) ? rand(0, 1) == 1 : false;
    }

    /**
     * Knight selects an item and uses it
     * @param $enemyDamage
     * @param $ownerDamage
     * @return string
     */
    public function useItem(&$enemyDamage, &$ownerDamage): string
    {
        $i = rand(0, count($this->inventory) - 1);
        $this->inventory[$i]->modificateDamage($enemyDamage, $ownerDamage);
        $itemName = $this->inventory[$i]->getName();
        if ($this->inventory[$i]->single_use) {
            unset($this->inventory[$i]);
            $this->inventory = array_values($this->inventory);
        }
        return $itemName;
    }

}