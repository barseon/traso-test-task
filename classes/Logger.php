<?php

namespace TurnBasedGame\GameTools;

use TurnBasedGame\Config;

/**
 * Class Logger
 * @package TurnBasedGame\GameTools
 */
class Logger
{
    /**
     * @var bool
     */
    private $fileWrite = false;
    /**
     * @var bool
     */
    private $consoleOutput = false;
    /**
     * @var string
     */
    private $outputFile = '';

    /**
     * Logger constructor.
     * @param $outputFile
     * @param bool $fileWrite
     * @param bool $consoleOutput
     */
    public function __construct($outputFile)
    {
        $this->fileWrite =  Config::SAVE_BATTLELOG_IN_FILE;
        $this->consoleOutput = Config::SHOW_BATTLELOG_IN_CONSOLE;
        $this->outputFile = $outputFile;
    }

    /**
     * @param $input
     */
    public function log($input): void
    {
        if ($this->fileWrite) {
            file_put_contents($this->outputFile, $input, FILE_APPEND);
        }
        if ($this->consoleOutput) {
            echo $input;
        }
    }

    public function removeLogFile(): void
    {
        unlink($this->outputFile);
    }

}

