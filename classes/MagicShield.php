<?php

namespace TurnBasedGame\GameObjects;

/**
 * Class MagicShield
 * @package TurnBasedGame\GameObjects
 */
class MagicShield extends Item
{
    /**
     * @var bool
     */
    public $single_use = true;
    /**
     * @var string
     */
    protected $name = '**Magic Shield**';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param $enemyDamage
     * @param $ownerDamage
     */
    public function modificateDamage(&$enemyDamage, &$ownerDamage): void
    {
        $ownerDamage += $enemyDamage;
        $enemyDamage = 0;
    }

}