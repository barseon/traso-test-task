<?php

namespace TurnBasedGame\GameObjects;

/**
 * Class MagicWand
 * @package TurnBasedGame\GameObjects
 */
class MagicWand extends Item
{
    /**
     * @var bool
     */
    public $single_use = true;
    /**
     * @var string
     */
    protected $name = '**Magic Wand**';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param $enemyDamage
     * @param $ownerDamage
     */
    public function modificateDamage(&$enemyDamage, &$ownerDamage): void
    {
        $ownerDamage = $ownerDamage * 2;
    }

}