<?php

namespace TurnBasedGame\GameObjects;

/**
 * Class Player
 * @package TurnBasedGame\GameObjects
 */
abstract class Player
{
    /**
     * @var string
     */
    public $name = '';
    /**
     * @var int
     */
    protected $hp = 100;

    /**
     * Player constructor.
     * @param $playerName
     */
    public function __construct($playerName)
    {
        $this->name = $playerName;
    }

    /**
     * Damage taken
     * @return mixed
     */
    abstract public function applyHit(); // Удар гладиатора

    /**
     * Damage received
     * @param $damage
     * @return mixed
     */
    abstract public function takeDamage($damage);

    /**
     * @return int
     */
    public function getHp(): int
    {
        return $this->hp;
    }

    /**
     * Is the knight alive?
     * @return int
     */
    public function isAlive(): int
    {
        return $this->hp > 0;
    }
}