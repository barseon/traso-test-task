<?php

namespace TurnBasedGame\GameObjects;

/**
 * Trait Round
 * @package TurnBasedGame\GameObjects
 */
trait Round
{
    public $knight1;
    public $knight2;
    public $knight1Hit;
    public $knight2Hit;
    public $knight1Item;
    public $knight2Item;
    public $roundResult;

    /**
     * @param $knight1
     * @param $knight2
     */
    public function setPlayers($knight1, $knight2)
    {
        $this->knight1 = $knight1;
        $this->knight2 = $knight2;
    }

    /**
     * @return object
     */
    public function beforeRound(): object
    {
        $this->knight1Hit = $this->knight1->applyHit();
        $this->knight2Hit = $this->knight2->applyHit();
        return $this;
    }

    /**
     * @return object
     */
    public function duringRound(): object
    {
        if ($this->knight1->wantUseItem()) {
            $this->knight1Item = $this->knight1->useItem($this->knight2Hit, $this->knight1Hit);
        }

        if ($this->knight2->wantUseItem()) {
            $this->knight2Item = $this->knight2->useItem($this->knight1Hit, $this->knight2Hit);
        }
        return $this;
    }

    /**
     * @return object
     */
    public function afterRound(): object
    {
        $this->knight2->takeDamage($this->knight1Hit);

        if ($this->knight2->isAlive()) {
            $this->knight1->takeDamage($this->knight2Hit);
        } else {
            $this->knight2Hit = 0;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function roundResult(): string
    {
        $this->roundResult = $this->knight1->name . (isset($this->knight1Item) ? ' (uses a ' . $this->knight1Item . ')' : '') .
            "\tvs\t" .
            $this->knight2->name . (isset($this->knight2Item) ? ' (uses a ' . $this->knight2Item . ')' : '') .
            "\t|\tdmg: " . $this->knight1Hit . '/' . $this->knight2Hit .
            "\t|\tHP: " . ($this->knight1->isAlive() ? $this->knight1->getHp() : 'dead') . '/' . ($this->knight2->isAlive() ? $this->knight2->getHp() : 'dead') . "\n";

        unset($this->knight1Item);
        unset($this->knight2Item);
        return $this->roundResult;
    }
}