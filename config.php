<?php

namespace TurnBasedGame;

/**
 * Class Config
 * @package TurnBasedGame
 */
class Config
{
    /**
     * Name of the game
     */
    const GAME_NAME = 'Knight Tournament';

    /**
     * How many default knights participate in the tournament if the user has not set this value (from console)
     */
    const DEFAULT_KNIGHT_NUMBER = 6;

    /**
     * Show battle progress in console
     */
    const SHOW_BATTLELOG_IN_CONSOLE = true;

    /**
     * Save battle results to file log.txt
     */
    const SAVE_BATTLELOG_IN_FILE = true;

    /**
     * Use real names for players from names.txt
     */
    const USE_HUMAN_NAMES = false;
}
